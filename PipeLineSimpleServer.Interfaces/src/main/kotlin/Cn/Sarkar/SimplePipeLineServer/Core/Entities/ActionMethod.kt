/**
Company: Sarkar software technologys
WebSite: http://www.sarkar.cn
Author: yeganaaa
Date : 11/1/17
Time: 9:09 AM
 */

package Cn.Sarkar.SimplePipeLineServer.Core.Entities

/*copied from JetBrains ktor framework, thankyou ktor team.*/
data class ActionMethod(val value: String) {
    companion object {
        val Get = ActionMethod("GET")
        val Post = ActionMethod("POST")
        val Put = ActionMethod("PUT")
        val Patch = ActionMethod("PATCH") // https://tools.ietf.org/html/rfc5789
        val Delete = ActionMethod("DELETE")
        val Head = ActionMethod("HEAD")
        val Options = ActionMethod("OPTIONS")

        fun parse(method: String): ActionMethod {
            return when (method) {
                Get.value -> Get
                Post.value -> Post
                Put.value -> Put
                Patch.value -> Patch
                Delete.value -> Delete
                Head.value -> Head
                Options.value -> Options
                else -> ActionMethod(method)
            }
        }

        val DefaultMethods = listOf(Get, Post, Put, Patch, Delete, Head, Options)
    }
}

/**
Company: Sarkar software technologys
WebSite: http://www.sarkar.cn
Author: yeganaaa
Date : 11/3/17
Time: 6:50 PM
 */

package Cn.Sarkar.SimplePipeLineServer.Core.Entities

data class SubjectParameter<TSubject>(var data: TSubject, var isHandled: Boolean = false, var isOkay: Boolean = false, var message: String = "")
/**
Company: Sarkar software technologys
WebSite: http://www.sarkar.cn
Author: yeganaaa
Date : 10/31/17
Time: 11:56 AM
 */

package Cn.Sarkar.SimplePipeLineServer.Core.Interfaces

import Cn.Sarkar.SimplePipeLineServer.Core.Utils.ValuesMap


interface FeatureContext {
    val request: ActionRequest
    val response: ActionResponse
    /*
        * Example: /User/{UserID}/Get
        * pathParameters["UserID"]
        * */
    var pathParameters: ValuesMap<String, String>
}
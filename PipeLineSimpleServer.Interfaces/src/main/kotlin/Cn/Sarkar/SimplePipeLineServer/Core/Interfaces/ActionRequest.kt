/**
Company: Sarkar software technologys
WebSite: http://www.sarkar.cn
Author: yeganaaa
Date : 11/5/17
Time: 11:49 AM
 */

package Cn.Sarkar.SimplePipeLineServer.Core.Interfaces

import Cn.Sarkar.SimplePipeLineServer.Core.Utils.ValuesMap
import Cn.Sarkar.SimplePipeLineServer.Core.Entities.ActionMethod
import Cn.Sarkar.SimplePipeLineServer.Core.Entities.ActionPath
import java.io.InputStream

interface ActionRequest{
    var path: ActionPath
    var queryParameters: ValuesMap<String, String>
    var headerParameters: ValuesMap<String, String>
    var cookies: ValuesMap<String, String>
    var bodyParameter: InputStream
    var method: ActionMethod
}
/**
Company: Sarkar software technologys
WebSite: http://www.sarkar.cn
Author: yeganaaa
Date : 11/3/17
Time: 9:22 PM
 */

package Cn.Sarkar.SimplePipeLineServer.Application

import Cn.Sarkar.Pipeline.Core.PipeLineFeature
import Cn.Sarkar.Pipeline.Extension.installFeature
import Cn.Sarkar.Pipeline.Extension.tryInstallFeature
import Cn.Sarkar.SimplePipeLineServer.Core.Entities.SubjectParameter
import Cn.Sarkar.SimplePipeLineServer.Core.Interfaces.FeatureContext

fun InfraStructure.install(feature: PipeLineFeature<SubjectParameter<Any>, FeatureContext>){
    pipeLine.installFeature(feature)
}

fun InfraStructure.tryInstall(feature: PipeLineFeature<SubjectParameter<Any>, FeatureContext>) : Boolean{
    return pipeLine.tryInstallFeature(feature)
}

fun InfraStructure.execute(featureProvider: FeatureContext, subject: SubjectParameter<Any>) : SubjectParameter<Any>{
    return pipeLine.execute(featureProvider, subject)
}
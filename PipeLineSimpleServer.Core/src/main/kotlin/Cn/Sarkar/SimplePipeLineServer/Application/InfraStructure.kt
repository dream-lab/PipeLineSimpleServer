/**
Company: Sarkar software technologys
WebSite: http://www.sarkar.cn
Author: yeganaaa
Date : 11/3/17
Time: 11:04 AM
 */

package Cn.Sarkar.SimplePipeLineServer.Application

import Cn.Sarkar.SimplePipeLineServer.Core.Entities.SubjectParameter
import Cn.Sarkar.SimplePipeLineServer.Core.Interfaces.FeatureContext
import Cn.Sarkar.SimplePipeLineServer.Core.PipeLines.PipeLineInfraStructure

abstract class InfraStructure {
    val pipeLine = PipeLineInfraStructure<Any>()
}
/**
Company: Sarkar software technologys
WebSite: http://www.sarkar.cn
Author: yeganaaa
Date : 11/3/17
Time: 12:16 PM
 */

package Cn.Sarkar.SimplePipeLineServer.Core.PipeLines

import Cn.Sarkar.Pipeline.Core.Info.PipeLineInfo
import Cn.Sarkar.Pipeline.Core.Phase
import Cn.Sarkar.Pipeline.Core.PipeLine
import Cn.Sarkar.SimplePipeLineServer.Core.Entities.SubjectParameter
import Cn.Sarkar.SimplePipeLineServer.Core.Interfaces.FeatureContext

class PipeLineInfraStructure<TSubject> : PipeLine<SubjectParameter<TSubject>, FeatureContext>(before, filter, routeBefore, routing, routeAfter, transform, after) {
    override val info: PipeLineInfo = PipeLineInfo(
            "Infira Structure PipeLine",
            "يادرولۇق بۆلەك، ھەر قانداق بىر بۇيرۇق يۈرگۈزۈلگەندە مۇشۇ تۇرۇبا دىن ئۆتىدۇ",
            "Sarkar Software Technologys",
            "yeganaaa",
            1,
            "v0.1"
    )

    companion object {
        /**
         * ھەر قانداق ئىلتىماس ئەڭ ئاۋال بۇ يەرگە كىلىدۇ، بۇ يەردە ئالدىنئالا بىر تەرەپ قىلىش ئىلىپ بارسا بولىدۇ
         * */
        val before = Phase("Before")
        /**
         * ئىلتىماسقا قارىتا سۈزۈش ئىلىپ بىرىش، سالاھىيەت دەلىللەش قاتارلىق سۈزۈش مەشغۇلاتى ئىلىپ بىرىلىدۇ
         * */
        val filter = Phase("Filter")
        /**
         *رەسمىي يوسۇندا يول باشلاشقا كىرىشتىن بۇرۇنقى تەييارلىق، مەسىلەن
         * Session
         * نى سانداندىن ئوقۇپ تەييار قىلىش دىگەندەك يول باشلاش رايۇنىغا كىرىشتىن ئاۋالقى تەييارلىق
         * */
        val routeBefore = Phase("RouteBefore")
        /**
         * يول باشلاش رايۇنى
         * Method, Query Parameter, Header Parameter Body Parameter
         * ۋە باشقا ئۇسۇللار ئارقىلىق مەلۇم بىر يول باشلىغۇچقا كىرىدۇ
         * */
        val routing = Phase("Routing")
        /**
         * يول باشلاش ئاخىرلاشقاندىن كىيىنكى رايۇن
         * يول باشلاش ئاخىرلاشقاندىن كىيىنكى بىر قىسىم مەشغۇلاتلار مۇشۇ يەردە قىلىنىدۇ
         * */
        val routeAfter = Phase("RouteAfter")
        /**
         * يول باشلاش ئاخىرلاشقاندىن كىيىن بۇ بۆلەكتە ئاخىرقى بىر قىسىم مەشغۇلاتلار قىلىنسا بولىدۇ
         * */
        val transform = Phase("Transform")
        /**
        * ئىلتىماس ئىجرا قىلىش تاماملانغاندىن كىيىن ئاخىرقى بۆلەككە كىرىدۇ
         * مەسىلەن بۇ يەردە ئاخىرقى نەتىجىنى ئانالىز قىلىش، ئاخىرقى نەتىجىنى بىسىپ چىقىرىش دىگەندەك مەشغۇلاتلار بولسا بولىدۇ
        * */
        val after = Phase("After")
    }
}
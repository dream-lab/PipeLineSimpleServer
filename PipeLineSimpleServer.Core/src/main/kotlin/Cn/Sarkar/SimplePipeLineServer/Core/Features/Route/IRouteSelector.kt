/**
Company: Sarkar software technologys
WebSite: http://www.sarkar.cn
Author: yeganaaa
Date : 11/3/17
Time: 5:18 PM
 */

package Cn.Sarkar.SimplePipeLineServer.Core.Features.Route

import Cn.Sarkar.Pipeline.Core.PipeLineContext

interface IRouteSelector<TSubject, TFeatureProvider> {
    fun PipeLineContext<TSubject, TFeatureProvider>.select() : Boolean
    override fun toString() : String
}